import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class JobScheduling {
    private static boolean conflict(Job job1, Job job2) {
        return (Float.compare(job1.getStartTime(), job2.getStartTime()) <= 0
                && Float.compare(job2.getStartTime(), job1.getFinishTime()) < 0)
                || (Float.compare(job2.getStartTime(), job1.getStartTime()) <= 0
                && Float.compare(job1.getStartTime(), job2.getFinishTime()) < 0);
    }

    private static List<Job> filterJobs(List<Job> jobs) {
        List<Job> res = new ArrayList<>();
        for (Job job1 : jobs) {
            boolean ok = true;
            for (Job job2 : res) {
                if (conflict(job1, job2)) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                res.add(job1);
            }
        }

        return res;
    }

    static List<Job> byShortestInterval(List<Job> jobs) {
        jobs.sort(Comparator.comparingDouble(o -> o.getFinishTime() - o.getStartTime()));
        return filterJobs(jobs);
    }

    static List<Job> byEarliestFinish(List<Job> jobs) {
        jobs.sort(Comparator.comparingDouble(Job::getFinishTime));
        return filterJobs(jobs);
    }

    static List<Job> byFewestConflicts(List<Job> jobs) {
        for (int i = 0; i < jobs.size(); i++) {
            for (int j = 0; j < jobs.size(); j++) {
                if (i != j && conflict(jobs.get(i), jobs.get(j))) {
                    jobs.get(i).incConflict();
                }
            }
        }
        jobs.sort(Comparator.comparingInt(Job::getConflict));
        return filterJobs(jobs);
    }

    static List<Job> byExhaustiveSearch(List<Job> jobs) {
        int n = jobs.size();
        int value = 0;
        int count = 0;
        for (int ms = 1; ms < (1 << n); ms++) {
            List<Integer> chosen = new ArrayList<>();
            for(int i = 0; i < n; i++) {
                if(((ms >> i) & 1) == 1) {
                    chosen.add(i);
                }
            }

            boolean ok = true;
            for(int i = 0; i < chosen.size(); i++) {
                for(int j = i + 1; j < chosen.size(); j++) {
                    if(conflict(jobs.get(chosen.get(i)), jobs.get(chosen.get(j)))) {
                        ok = false;
                        break;
                    }
                }
                if(!ok) {
                    break;
                }
            }

            if(ok && chosen.size() > count) {
                count = chosen.size();
                value = ms;
            }
        }

        List<Job> ans = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            if(((value >> i) & 1) == 1) {
                ans.add(jobs.get(i));
            }
        }

        return ans;
    }
}
