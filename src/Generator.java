import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

class Generator {
    static void generate(String testPath, String testPrefix, int nTests, int maxIntervals) {
        for (int i = 0; i < nTests; i++) {
            Path path = Paths.get(testPath, testPrefix + "_" + i);
            new File(path.toString()).mkdirs();
            try {
                List<Job> jobs = new ArrayList<>();
                for (int j = 0; j < maxIntervals; j++) {
                    int s = ThreadLocalRandom.current().nextInt(1, 100000000);
                    int f = ThreadLocalRandom.current().nextInt(1, 100000000);
                    if (s > f) {
                        int tmp = s;
                        s = f;
                        f = tmp;
                    }
                    if (s == f) {
                        f = s + 1;
                    }
                    jobs.add(new Job(j + 1, s, f));
                }
                int len = JobScheduling.byExhaustiveSearch(new ArrayList<>(jobs)).size();
                FileWriter writer = new FileWriter(path.toString() + "/input.txt");
                PrintWriter printWriter = new PrintWriter(writer);
                for (Job job : jobs) {
                    printWriter.printf("%d %d %d\n", job.getId(), (int) job.getStartTime(), (int) job.getFinishTime());
                }
                printWriter.close();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
