import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

class Checker {
    static void check(String testPath) {
        try {
            final Statistic stats = new Statistic();
            Files.list(Paths.get(testPath)).forEach(p -> {
                try {
                    InputStream input = new FileInputStream(p + "/input.txt");
                    List<Job> jobs = loadJobs(input);
                    input.close();
                    List<Job> shortestInterval = byShortestInterval(jobs);
                    List<Job> earliestFinish = byEarliestFinish(jobs);
                    List<Job> fewestConflicts = byFewestConflicts(jobs);
                    List<Job> exhaustiveSearch = byExhaustiveSearch(jobs);
                    stats.incTest();
                    if (shortestInterval.size() < exhaustiveSearch.size()) {
                        stats.incShortestInterval();
                        stats.setErrShortestInterval(exhaustiveSearch.size() - shortestInterval.size());
                    }
                    if (earliestFinish.size() < exhaustiveSearch.size()) {
                        stats.incEarliestFinish();
                        stats.setErrEarliestFinish(exhaustiveSearch.size() - earliestFinish.size());
                    }
                    if (fewestConflicts.size() < exhaustiveSearch.size()) {
                        stats.incFewestConflicts();
                        stats.setErrFewestConflicts(exhaustiveSearch.size() - fewestConflicts.size());
                    }
                    outputJobs(p.toString() + "/by_shortest_interval.txt", shortestInterval);
                    outputJobs(p.toString() + "/by_earliest_finish.txt", earliestFinish);
                    outputJobs(p.toString() + "/by_fewest_conflicts.txt", fewestConflicts);
                    outputJobs(p.toString() + "/by_exhaustive_search.txt", exhaustiveSearch);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            System.out.printf("Finished %d tests\n", stats.nTests);
            System.out.println("Shortest interval:");
            System.out.printf("Correct answers:%d/%d\n", stats.nTests - stats.nShortestInterval, stats.nTests);
            System.out.printf("Maximum error:%d\n", stats.errShortestInterval);
            System.out.printf("Sum error:%d\n", stats.sumShortestInterval);
            System.out.println();
            System.out.println("Earliest finish:");
            System.out.printf("Correct answers:%d/%d\n", stats.nTests - stats.nEarliestFinish, stats.nTests);
            System.out.printf("Maximum error:%d\n", stats.errEarliestFinish);
            System.out.printf("Sum error:%d\n", stats.sumEarliestFinish);
            System.out.println();
            System.out.println("Fewest conflicts:");
            System.out.printf("Correct answers:%d/%d\n", stats.nTests - stats.nFewestConflicts, stats.nTests);
            System.out.printf("Maximum error:%d\n", stats.errFewestConflicts);
            System.out.printf("Sum error:%d\n", stats.sumFewestConflicts);
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Job> byShortestInterval(List<Job> jobs) {
        return JobScheduling.byShortestInterval(new ArrayList<>(jobs));
    }

    private static List<Job> byEarliestFinish(List<Job> jobs) {
        return JobScheduling.byEarliestFinish(new ArrayList<>(jobs));
    }

    private static List<Job> byFewestConflicts(List<Job> jobs) {
        return JobScheduling.byFewestConflicts(new ArrayList<>(jobs));
    }

    private static List<Job> byExhaustiveSearch(List<Job> jobs) {
        return JobScheduling.byExhaustiveSearch(new ArrayList<>(jobs));
    }

    private static void outputJobs(String path, List<Job> jobs) {
        try {
            FileWriter writer = new FileWriter(path);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.println(jobs.size());
            printWriter.println(jobs);
            printWriter.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Job> loadJobs(InputStream stream) {
        Scanner scanner = new Scanner(stream);
        List<Job> jobs = new ArrayList<>();
        while (scanner.hasNext()) {
            int id = scanner.nextInt();
            float startTime = scanner.nextFloat();
            float finishTime = scanner.nextFloat();
            jobs.add(new Job(id, startTime, finishTime));
        }
        return jobs;
    }

    private static class Statistic {
        private int nTests;
        private int nShortestInterval;
        private int nEarliestFinish;
        private int nFewestConflicts;
        private int errShortestInterval;
        private int errEarliestFinish;
        private int errFewestConflicts;
        private int sumShortestInterval;
        private int sumEarliestFinish;
        private int sumFewestConflicts;

        private Statistic() {
            nTests = 0;
            nShortestInterval = 0;
            nEarliestFinish = 0;
            nFewestConflicts = 0;
            errShortestInterval = 0;
            errEarliestFinish = 0;
            errFewestConflicts = 0;
            sumShortestInterval = 0;
            sumEarliestFinish = 0;
            sumFewestConflicts = 0;
        }

        private void incTest() {
            nTests++;
        }

        private void incShortestInterval() {
            nShortestInterval++;
        }

        private void incEarliestFinish() {
            nEarliestFinish++;
        }

        private void incFewestConflicts() {
            nFewestConflicts++;
        }

        private void setErrShortestInterval(int value) {
            errShortestInterval = Math.max(errShortestInterval, value);
            sumShortestInterval += value;
        }

        private void setErrEarliestFinish(int value) {
            errEarliestFinish = Math.max(errEarliestFinish, value);
            sumEarliestFinish += value;
        }

        private void setErrFewestConflicts(int value) {
            errFewestConflicts = Math.max(errFewestConflicts, value);
            sumFewestConflicts += value;
        }
    }
}
