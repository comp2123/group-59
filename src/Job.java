public class Job {
    private final int id;
    private final float startTime;
    private final float finishTime;
    private int conflict;

    Job(int id, float startTime, float finishTime) {
        this.id = id;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.conflict = 0;
    }

    int getId() {
        return id;
    }

    float getStartTime() {
        return startTime;
    }

    float getFinishTime() {
        return finishTime;
    }

    int getConflict() {
        return conflict;
    }

    void incConflict() {
        conflict++;
    }

    public String toString() {
        return "(" + id + ", " + startTime + ", " + finishTime + ")";
    }
}
